const player = require('play-sound')(opts = {})
const EventEmitter = require('events');
const fs = require('fs');
const path = require('path');

class Player extends EventEmitter {
	play(videoId) {
		let self = this;

		if (this.proc)
		{
			this.dontEmit = true;
			this.proc.kill();
		}

		this.proc = player.play(path.join(__dirname, "library/" + videoId + ".wav"), function () {
			if (self.dontEmit)
			{
				self.dontEmit = false;
				return;
			}
			delete self.proc;
			self.emit("end");
		});

	}

	stop() {
		if (this.proc)
			this.proc.kill();
	}

	startup() {
		player.play(path.join(__dirname, "startup.wav"), function () {});
	}

	get isPlaying() {
		return this.proc != undefined;
	}
}

exports.Player = Player;