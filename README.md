# PiFm Server
This program wraps [PiFm](https://github.com/rm-hull/pifm) in a small server to allow playing YouTube songs on a radio. Once installed on a Raspberry Pi, you can navigate to the Pi's IP address and queue any YouTube video.

<!-- TOC -->

- [Dependencies](#dependencies)
- [Hardware](#hardware)
- [Usage](#usage)
- [Configuration](#configuration)

<!-- /TOC -->


## Dependencies
- NodeJS
- FFMPEG

## Hardware
PiFm uses GPIO 4 (pin 7) on the Raspberry Pi to broadcast radio. To increase the range, you should plug a small wire to this PIN.

## Usage
```
npm install
npm start
```
You can then connect a radio to 102.1 FM, a web browser to you Raspberry Pi and enjoy your music 🙂

## Configuration
You can create a `config.json` file to set the frequency of the broadcast and the server's port. It should look like this :
```json
{
	"port": 80,
	"frequency": 102.1
}
```