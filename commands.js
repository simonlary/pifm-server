const fs = require('fs');
const path = require('path');
const ytdl = require('ytdl-core');
const https = require('https');
const cp = require('child_process');
const WebSocket = require('ws');

// Choose dev or prod Player depending on NODE_ENV
let playerFile = (process.env.NODE_ENV == "development") ? "player-test.js" : "player.js";
const Player = require(path.join(__dirname, playerFile)).Player;

let queue = [];
let downloading = {};
let loop = false;

let ws;

let player = new Player();
player.on("end", function () {
	PlayNext();
});

// Create folders if first time
if (fs.existsSync(path.join(__dirname, "downloading")) == false)
	fs.mkdirSync(path.join(__dirname, "downloading"));
if (fs.existsSync(path.join(__dirname, "converting")) == false)
	fs.mkdirSync(path.join(__dirname, "converting"));
if (fs.existsSync(path.join(__dirname, "library")) == false)
	fs.mkdirSync(path.join(__dirname, "library"));
if (fs.existsSync(path.join(__dirname, "library/data.json")) == false)
	fs.writeFileSync(path.join(__dirname, "library/data.json"), "{}");

let library = JSON.parse(fs.readFileSync(path.join(__dirname, "library/data.json")));
let folder = fs.readdirSync(path.join(__dirname, "library"));
folder.forEach(file => {
	let parts = file.split('.');
	if (parts.length == 2)
	{
		if (parts[1] == "wav" && library[parts[0]] == undefined)
		{
			fs.unlinkSync(path.join(__dirname, "library/", file));
		}
	}
	else
	{
		console.warn("Weird file in library");
	}
});
fs.readdirSync(path.join(__dirname, "downloading")).forEach(file => fs.unlinkSync(path.join(__dirname, "downloading/", file)));
fs.readdirSync(path.join(__dirname, "converting")).forEach(file => fs.unlinkSync(path.join(__dirname, "converting/", file)));

const Play = function () {
	if (queue.length == 0)
	{
		player.stop();
		return;
	}
	console.log("Now playing : " + library[queue[0]]);
	player.play(queue[0]);
};

const PlayNext = function () {
	if (queue.length == 0)
	{
		player.stop();
		return;
	}
	if (loop)
	{
		queue.push(queue.shift())
	}
	else
	{
		queue.shift();
	}
	Play();
};

const DownloadSong = function (url) {
	ytdl.getInfo(url, function (err, info) {
		if (library[info.video_id] != undefined)
		{
			console.log("'" + info.title + "' (" + info.video_id + ") is already in the library.");
			return;
		}
		if (downloading[info.video_id] != undefined)
		{
			console.log("'" + info.title + "' (" + info.video_id + ") is already being downloaded.");
			return;
		}

		console.log("Starting download of '" + info.title + "' (" + info.video_id + ")");
		downloading[info.video_id] = info.title;
		UpdateClients();
		let stream = ytdl.downloadFromInfo(info, { filter: "audioonly" }).pipe(fs.createWriteStream(path.join(__dirname, "downloading/" + info.video_id + ".flv")));
		stream.on("finish", function () {

			console.log("Starting conversion of '" + info.title + "' (" + info.video_id + ")");
			let orig = path.join(__dirname, "downloading/" + info.video_id + ".flv");
			let dest = path.join(__dirname, "converting/" + info.video_id + ".wav");
			let dest2 = path.join(__dirname, "library/" + info.video_id + ".wav");
			cp.exec("ffmpeg -i " + orig + " -ar 44.1k -ac 1 " + dest, function (error, stdout, stderr) {
				fs.unlink(orig, function () { });
				fs.rename(dest, dest2, function () {
					AddToLibrary(info.video_id, info.title);
					console.log("'" + info.title + "' (" + info.video_id + ") added to the library.");
				});
			})
		});
	});
};

const AddToLibrary = function (videoId, title) {
	library[videoId] = title;
	delete downloading[videoId];
	fs.writeFile(path.join(__dirname, "library/data.json"), JSON.stringify(library), function (err) { if (err) throw err; });
	UpdateClients();
};

const Data = function () {
	return JSON.stringify({
		"queue": queue.map(function (curr) {
			return library[curr];
		}),
		"library": library,
		"downloading": downloading,
		"loop": loop
	});
};

const UpdateClients = function () {
	let data = Data();
	ws.clients.forEach(function (client) {
		if (client.readyState === WebSocket.OPEN)
		{
			client.send(data);
		}
	});
};

exports.commands = {
	/////////////
	//  MUSIC  //
	/////////////
	"downloadSong": {
		process: function (args) {
			if (args.search == undefined)
			{
				return;
			}

			if (args.search.match(/https:\/\/www.youtube.com\/watch\?v=/g) != null)
			{
				// YouTube video URL
				DownloadSong(args.search);
			}
			else // Search query
			{
				// Download the YouTube search page for that search term
				https.get('https://www.youtube.com/results?search_query=' + encodeURI(args.search), function (response) {
					response.setEncoding('utf8');
					let data = "";
					response.on('data', function (chunk) {
						data += chunk;
					}).on('end', function () {
						// Find the video ID
						let position = data.indexOf("/watch?v=");
						let videoId = data.slice(position + 9, position + 20);
						DownloadSong("https://www.youtube.com/watch?v=" + videoId);
					})
				})
			}
		}
	},
	"removeSong": {
		process: function (args) {
			if (args.videoId != undefined && library[args.videoId] != undefined)
			{
				let id = args.videoId;
				let np = queue[0];
				queue = queue.filter(s => s != id);
				if (queue[0] != np)
				{
					Play();
				}
				let songPath = path.join(__dirname, "library/" + id + ".wav");
				fs.unlink(songPath, function () { });
				delete library[id];
				fs.writeFile(path.join(__dirname, "library/data.json"), JSON.stringify(library), function (err) { if (err) throw err; });
				UpdateClients();
			}
		}
	},
	"playSong": {
		process: function (args) {
			if (args.videoId == undefined || library[args.videoId] == undefined)
			{
				return;
			}
			queue.push(args.videoId);
			// Start playback if not already playing
			if (player.isPlaying == false)
			{
				Play();
			}
			UpdateClients();
		}
	},
	"playAllSongs": {
		process: function (args) {
			Object.keys(library).forEach(id => queue.push(id));
			// Start playback if not already playing
			if (player.isPlaying == false)
			{
				Play();
			}
			UpdateClients();
		}
	},
	"playStartupSound": {
		process: function (args) {
			// Start playback if not already playing
			if (player.isPlaying == false)
			{
				player.startup();
			}
		}
	},
	"skip": {
		process: function (args) {
			PlayNext();
			UpdateClients();
		}
	},
	"shuffle": {
		process: function (args) {
			if (queue.length > 2)
			{
				for (let i = queue.length - 1; i > 1; i--)
				{
					let j = Math.floor((Math.random() * i) + 1);
					let tmp = queue[i];
					queue[i] = queue[j]
					queue[j] = tmp;
				}
			}
			UpdateClients();
		}
	},
	"loop": {
		process: function (args) {
			loop = args.loop;
			UpdateClients();
		}
	},
	"clear": {
		process: function (args) {
			queue = [];
			player.stop();
			UpdateClients();
		}
	}
};

exports.init = function (wsServer) {
	ws = wsServer;
	ws.on('connection', function (conn) {
		conn.send(Data());
	});
};