const fs = require('fs');
const path = require('path');

let config = {
	"port": 80,
	"frequency": 102.1
};

if (fs.existsSync(path.join(__dirname, "config.json")) == false)
	fs.writeFileSync(path.join(__dirname, "config.json"), JSON.stringify(config));

const file = JSON.parse(fs.readFileSync(path.join(__dirname, "config.json")));

for (let option in config)
{
	if (file[option] != undefined)
		config[option] = file[option];
}

module.exports = config;