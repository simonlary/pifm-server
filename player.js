const cp = require('child_process');
const EventEmitter = require('events');
const fs = require('fs');
const path = require('path');
const config = require(path.join(__dirname, "pifm-config.js"));

class Player extends EventEmitter {
	play(videoId) {
		let self = this;

		if (this.proc)
		{
			this.dontEmit = true;
			this.proc.kill();
		}

		// TODO : Améliorer la qualité (stéréo?)
		self.proc = cp.execFile(path.join(__dirname, "pifm"), [path.join(__dirname, "library/" + videoId + ".wav"), config.frequency, 44100], function () {
			if (self.dontEmit)
			{
				self.dontEmit = false;
				return;
			}
			delete self.proc;
			self.emit("end");
		});

	}

	startup() {
		cp.execFile(path.join(__dirname, "pifm"), [path.join(__dirname, "startup.wav"), config.frequency, 44100], function () { });
	}

	stop() {
		if (this.proc)
			this.proc.kill();
	}

	get isPlaying() {
		return this.proc != undefined;
	}
}

exports.Player = Player;