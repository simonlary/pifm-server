const exec = require('child_process').exec;

if (process.env.NODE_ENV != "development")
{
	exec("git clone https://github.com/rm-hull/pifm.git && mv pifm pifm-master && cd pifm-master && make && mv pifm ..", function (error, stdout, stderr) {
		if (error)
			console.error("Error with the compilation of pifm : " + error);
	});
}