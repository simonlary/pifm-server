const fs = require('fs');
const path = require('path');
const http = require('http');
const { URL } = require('url');
const WebSocket = require('ws');
const config = require(path.join(__dirname, "pifm-config.js"));

let commands = {};

const CheckMessageForCommand = function (searchParams) {
	let cmd = searchParams.command;
	if (cmd != null && commands[cmd] != undefined)
	{
		delete searchParams.command;
		commands[cmd].process(searchParams);
	}
};

const ParseHTTPParameters = function (url) {
	let params = {};
	new URL(url, "http://localhost/").searchParams.forEach((value, name, searchParams) => {
		params[name] = value;
	});
	return params;
};

////////////////////
//  LOAD COMMANDS //
////////////////////
let src = require("./commands.js");
for (let cmd in src.commands)
{
	commands[cmd] = src.commands[cmd];
}

/////////////////////
//  LOAD HTML FILE //
/////////////////////
const controllerHTML = fs.readFileSync(path.join(__dirname, "controller/controller.html"));
let controllerCSS = fs.readFileSync(path.join(__dirname, "controller/controller.css"), 'utf-8');
controllerCSS = (process.env.NODE_ENV == "development") ? (controllerCSS.replace("#262626", "#3A0000")) : controllerCSS;
const controllerJS = fs.readFileSync(path.join(__dirname, "controller/controller.js"));

///////////
//  MAIN //
///////////
const httpServer = http.createServer(function (req, res) {
	let content = "";

	if (req.url === "/") // If url is empty, just send the WebApp
	{
		content = controllerHTML;
		res.writeHead(200, { 'Content-type': 'text/html' });
	}
	else if (req.url === "/controller.css") // Else, check if it's the css
	{
		content = controllerCSS;
		res.writeHead(200, { 'Content-type': 'text/css' });
	}
	else if (req.url === "/controller.js") // or the js
	{
		content = controllerJS;
		res.writeHead(200, { 'Content-type': 'text/javascript' });
	}
	else // Else, check if it's a HTTP command
	{
		CheckMessageForCommand(ParseHTTPParameters(req.url));
		res.writeHead(200, { 'Content-type': 'text/plain' });
	}

	res.write(content);
	res.end();
});

const wsServer = new WebSocket.Server({ server: httpServer });
wsServer.on('connection', function (ws, req) {
	ws.on('message', function (message) {
		CheckMessageForCommand(JSON.parse(message))
	});
});
src.init(wsServer);

httpServer.listen(config.port);

commands["playStartupSound"].process();
console.log('Ready!');