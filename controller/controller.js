let currentLibrary = "";
let loop = false;
let socket;

const init = function () {
	// Open WebSocket connection
	socket = new WebSocket("ws://" + location.host);

	socket.addEventListener("message", function (event) {
		Update(JSON.parse(event.data));
	});
};

const request = function (parameters, callback) {
	socket.send(JSON.stringify(parameters))
};

const Update = function (data) {
	// Update Queue
	if (data.queue.length == 0)
	{
		document.querySelector("#player ul").innerHTML = "<li class=\"list-group-item text-center list-group-item-primary\">Nothing in queue!</li>";
	}
	else
	{
		document.querySelector("#player ul").innerHTML = data.queue.map((curr, index) => {
			return (index == 0) ? "<li class=\"list-group-item list-group-item-primary\">" + curr + "</li>" : "<li class=\"list-group-item list-group-item-secondary\">" + curr + "</li>";
		}).join("");
	}

	// Update Library (and downloading)
	if (data.library.length == 0 && data.downloading.length == 0)
	{
		document.querySelector("#library ul").innerHTML = "<li class=\"list-group-item text-center list-group-item-secondary\">Nothing in library!</li>";
	}
	else
	{
		let content = Object.keys(data.library).map(function (curr) {
			return "<li class=\"list-group-item list-group-item-secondary\" data-video-title=\"" + data.library[curr].toLowerCase() + "\">"
				+ data.library[curr]
				+ "<span class=\"btn-group btn-group-sm float-right\">"
				+ "<button class=\"btn btn-danger\" onclick=\"removeSong('" + curr + "');\"><i data-feather=\"x\"></i></button>"
				+ "<button class=\"btn btn-success\" onclick=\"addToQueue('" + curr + "');\"><i data-feather=\"plus\"></i></button>"
				+ "</span>"
				+ "</li>";
		}).join("");
		content += Object.keys(data.downloading).map(function (curr) {
			return "<li class=\"list-group-item list-group-item-secondary\">"
				+ data.downloading[curr]
				+ "<span class=\"loader float-right\">"
				+ "</span>"
				+ "</li>";
		}).join("");
		document.querySelector("#library ul").innerHTML = content;
		feather.replace();

		// Add some loaders if needed
		let loaders = document.getElementsByClassName("loader");
		let loader = document.getElementById("loader");
		for (let l of loaders)
		{
			let clone = loader.cloneNode(true);
			clone.id = "";
			l.appendChild(clone);
		}
	}

	// Update Loop
	loop = data.loop;
	document.getElementById("loop").className = loop ? "btn btn-info" : "btn btn-primary";
};

const addToQueue = function (videoId) {
	request({ "command": "playSong", "videoId": videoId }, function () { });
};

const removeSong = function (videoId) {
	if (confirm("Are you sure you want to remove this song?"))
	{
		request({ "command": "removeSong", "videoId": videoId }, function () { });
	}
};

const addAllToQueue = function () {
	request({ "command": "playAllSongs" }, function () { });
};

const downloadSong = function () {
	request({ "command": "downloadSong", "search": document.getElementById("search").value }, function () { });
	document.getElementById("search").value = "";
};

const skip = function () {
	request({ "command": "skip" }, function () { });
};

const shuffle = function () {
	request({ "command": "shuffle" }, function () { });
};

const clearQueue = function () {
	request({ "command": "clear" }, function () { });
};

const toggleLoop = function () {
	loop = !loop;
	document.getElementById("loop").className = loop ? "btn btn-info" : "btn btn-primary";
	request({ "command": "loop", "loop": loop }, function () { });
};

const searchEnter = function () {
	if (event.keyCode == 13)
	{
		downloadSong();
	}
};

const updateFilter = function (input) {
	let entries = document.querySelectorAll('[data-video-title]');
	for (let e of entries)
	{
		if (e.dataset.videoTitle.indexOf(input.value.toLowerCase()) == -1)
		{
			e.classList.add("d-none");
		} else
		{
			e.classList.remove("d-none");
		}
	}
};